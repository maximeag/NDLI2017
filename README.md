SUPERDD:
* AGUSTI Maxime
* JONES Marie

# Application de signalisation d'accident de la circulation

## L'idée

Le système que nous avons imaginé est une application mobile permettant de signaler un accident de la route. Un opérateur qui se trouve dans un centre de secours sera notifié de cet accident grâce à un portail web intéractif en temps réel.

Le signalement d'un accident se fait en plusieurs étapes :
* Appuyer sur le bouton "Signaler un accident"
* Répondre à quelques questions pour connaître les détails de la situation

Lorsque l'informateur de l'accident de la circulation appuie sur le bouton "Signaler un accident", l'opérateur reçoit directement une notification avant même que l'informateur ait répondu aux questions. Lorsqu'une question est répondue, l'opérateur reçoit directement la réponse.

## Maquettes

Nous avons réalisé des maquettes correspondant à l'idée évoquée ci-dessus afin d'obtenir un premier prototype permettant de vérifier la réalisabilité de notre projet. 

![maquette desktop](screenshots/maquette_desktop.jpg)
![maquette mobile](screenshots/maquette_mobile.jpg)

## Architecture 

Le système est composé de trois applications :

* Une application Android permettant de signaler un accident de la route
* Un site web permettant aux opérateurs des centres d'urgence d'être informé
* Un service web

![architecture](screenshots/architecture.svg)

## Scénarios utilisateurs

### Cas 1 : Marie, témoin d'un accident

Nous sommes samedi soir et je rentre d'une soirée chez des amis à Villers-Châtel, petit village de campagne, vers 4h du matin. Il fait du brouillard, la route est étroite et sinueuse. Sur le chemin, entre Camblain l'Abbé et Mont Saint-Eloi, j'aperçois une voiture qui a percuté un arbre sur le bas côté. Je m'arrête en sécurité et sors mon téléphone portable aifn de prévenir les secours. Je lance l'application prévue à cet effet. Je clique sur le bouton afin de signaler un accident. Ma géolocalisation est automatiquement envoyée à la plateforme correspondante. Je réponds aux différentes questions qui me sont demandées : Je suis témoin, il s'agit d'une voiture qui a percuté un objet, il y a deux passagers tout deux inconscients, la voiture ne prend pas feu. Une fois ceci fait, un appel est passé automatiquement de mon téléphone afin de me mettre en contact avec le service de secours le plus adapté dans cette situation. Je confirme les informations données et éclaire certains points avec mon interlocuteur. On me demande d'attendre l'arrivée des secours, ce que je fais.

### Cas 2 : Olivier, passager

Nous sommes lundi matin et comme tous les matins, je pars en covoiturage avec mon collègue Maxime pour rejoindre notre lieu de travail. La route est plutôt calme comme tous les matins. Le paysage est agréable nous longeons une forêt. Soudain, une biche apparaît et Maxime tente de l'éviter mais nous nous retrouvons à heurter la voiture qui arrivait en face. Je met quelques temps à reprendre mes esprits et me rend compte que mon collègue et l'autre conducteur sont tous les deux inconscients. Je décide de contacter les secours. Je sors mon téléphone et démarre l'application à l'aide de mon assistance vocale. Je clique sur le bouton afin de signaler un accident, réponds aux différentes questions posées : Je suis impliqué dans l'accident, je suis blessé/en état de choc, il y a 3 personnes impliquées. Un appel est passé depuis mon téléphone pour me mettre en contact avec un interlocuteur. On me confirme l'arrivée imminente des secours. 

### Cas 3: Maxime, opérateur au SAMU

Nous sommes jeudi après-midi, comme souvent l'après-midi est mouvementée. Maxime reçoit un appel d'une personne ayant été témoin d'un accident sur l'A1. Le logiciel est directement relié au téléphone de l'opérateur ce qui permet de récupérer le numéro de téléphone de l'appelant et ainsi afficher le problème à l'écran. Maxime vérifie deux trois informations avec la personne appelante et envoie l'équipe de secours sur le lieu de l'accident. Il passe à l'appel suivant.

## Principe de l'application

* Signaler un accident 
* Répondre à des questions préliminaires pour orienter plus facilement les secours
* Donner des conseils sur la sécurisation des lieus, les gestes à faire ou pas ... 

### Questions posées par l'application

Nous avons à partir des conseils prodigués par la Croix Rouge sur les quatre étapes pour porter secours(http://www.croix-rouge.fr/Je-me-forme/Particuliers/Les-6-gestes-de-base/Les-4-etapes-pour-porter-secours), établi une série de questions posées par l'application et dont les réponses sont transmises en amont sur la plateforme. 

![diagramme de flux](screenshots/flowchart-diagram.png)

## Diagramme de classes

### Application mobile 

L'application a été développée avec le Android SDK. 

Nous utilisons GoogleMaps API pour afficher la position de l'utilisateur. Cette API, comme toutes les APIs Google nécessite une clé qui ne peut pas être rendue publique. Nous vous mettons donc à disposition un APK afin que vous puissiez tester. Toutefois, si vous souhaitez compiler l'application, il vous faudra une clé d'API, l'ensemble des étapes à suivre est disponible à l'adresse suivante : https://developers.google.com/maps/documentation/android-api/start?hl=fr.

Nous utilisons également SmartLocation pour récupérer la position de l'utilisateur et Retrofit pour communiquer avec le service Web. 

Nous avons choisi d'utiliser la synthèse vocal pour énoncer les questions à voix haute. En effet, il est possible que dans certaines situations, il soit plus facile pour un utilisateur d'écouter la question plutôt que de la lire.

Le package model contient toutes les classes représentant des objets qui vont être sérialisés avant d'être envoyé ou reçu par le service web. Le package service contient toutes les interfaces définissant les actions possibles depuis l'application sur le client (GET/POST une ressource).
RetrofitClient est un singleton. Cela nous permet de n'utiliser qu'une seule instance de Retrofit dans toutes l'application et de facilement créer et requêter des services. 
Le package activity contient les différentes activités permettant l'interaction avec les données. 

* MainActivity : activité principale, affiche la carte indiquant la position et permet le signalement d'un accident
* QuestionActivity : affiche les différentes questions et permet d'y répondre
* PhoneNumberActivity : affichage en cas d'erreur de réseau, de serveur etc, affiche les différents numéros d'urgence 
* CallActivity : activité en charge d'appeler automatiquement le service de secours adapté (ce service a été mocké pour des raisons évidentes)

![diagramme Application mobile](screenshots/App.svg)

#### Aperçu

![accueil](screenshots/accueil.jpg)
![question](screenshots/question.jpg)
![appel](screenshots/appel.jpg)


### Site Web

Nous utilisons React JS avec TypeScript pour développer le site. Pour des raisons rechniques, nous n'avons malheureusement pas pu déployer le code correspondant. Toutefois, les sources sont disponible dans le projet NDLI2017-Desktop. 

 L'interface se décompose en deux parties : 

* Un affichage de la carte pour visualiser le lieu de l'accident
* Un encart permettant les différentes informations sur l'utilisateur : numéro de téléphone, réponse au questionnaire 

#### Aperçu

![desktop](screenshots/Client-Opérateur.png)

### Web service

Le Web Service a été développé à l'aide de Spring en lien avec une Base De données MySQL. La configuration est disponible dans le `docker-compose.yml`. Pour communiquer en temps réel avec l'API nous utilisons Socket.io.  

Les packages ont été découpés par entités métier.
* alert : relatif aux signalements d'accidents
* question : relatif aux questions et réponses données par les utilisateurs
* user : realtif aux utilisateurs identifiés par leur numéro de téléphone

![diagramme Web service](screenshots/class-diagram.svg)


#### URIs appelables 

Méthode | URI | Description
--------|-----|-------------
POST | /api/alerts | permet de signaler un nouvel accident
GET | /api/questions | permet de récupérer le questionnaire à poser
POST | /api/answers | permet d'envoyer les réponses du questionnaires

## Possibilités d'évolution 

* Envoi des coordonnées GPS depuis la plateforme Web vers le télephone/bip des équipes de secours pour avoir directement l'itinéraire GPS

* Intégrer des éléments permettant la sécurisation des lieux et/ou des gestes de premier secours à réaliser